import type { Serverless } from 'serverless/aws';
import { accountId, stage } from './config';

// useful variables
const region = process.env.AWS_REGION || 'ap-northeast-1';
const serviceName = 'mougi-sms-trigger-service';
const {
  MOUGI_API_KEY,
} = process.env;

console.log(process.env);

const mougiAwsStackOutput = (outputName: string) => `\${cf:mougi-aws-infra-${stage}.${outputName}}`;

const serverlessConfiguration: Serverless = {
  service: {
    name: serviceName,
  },
  frameworkVersion: '2',
  custom: {
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: true,
      packager: 'yarn'
    },
    logRetentionInDays: stage === 'prd' ? 90 : 7,
    // importApiGateway: {
    //   name: `${stage}-mougi-v1-api`,
    // },
    // infraService: `mougi-aws-infra-${stage}`,
    customDomain: {
      domainName: 'sms-trigger.mougi.com',
      stage: stage,
      certificateName: '*.mougi.com',
      createRoute53Record: true,
      endpointType: 'edge',
      securityPolicy: 'tls_1_2',
      autoDomain: true,
      enable: true,
    },
    prune: { automatic: true, number: 5 },
    newRelic: {
      linkedAccount: `mougi-${stage}`,
      accountId: 2996641,
      apiKey: 'NRAK-6IMY8NE8T4U6OWD92NPB8NFGSIQ',
      enableExtension: true,
      enableIntegration: true,
    }
  },
  // Add the serverless-webpack plugin
  plugins: [
    'serverless-plugin-typescript',
    'serverless-offline',
    'serverless-import-apigateway',
    'serverless-prune-plugin',
    'serverless-domain-manager',
    'serverless-newrelic-lambda-layers',
    'serverless-plugin-log-retention',
  ],
  provider: {
    name: 'aws',
    runtime: 'nodejs12.x',
    region,
    apiGateway: {
      minimumCompressionSize: 1024,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      STAGE: stage,
      MOUGI_API_KEY,
    },
    stage,
    vpc: {
      securityGroupIds: [mougiAwsStackOutput('privateSGId')],
      subnetIds: [mougiAwsStackOutput('privateSubnet1Id'), mougiAwsStackOutput('privateSubnet2Id')],
    },
    iamRoleStatements: [
      {
        Effect: 'Allow',
        Action: [
          'ec2:CreateNetworkInterface',
          'ec2:AttachNetworkInterface',
          'ec2:DescribeNetworkInterfaces',
          'ec2:DeleteNetworkInterface',
          'lambda:InvokeFunction',
          'SNS:Publish',
          'execute-api:Invoke',
          'logs:CreateLogGroup',
          'logs:CreateLogStream',
          'logs:PutLogEvents'
        ],
        Resource: '*'
      }, {
        Sid: 'SMAccess',
        Effect: 'Allow',
        Action: [
          'secretsmanager:GetSecretValue',
        ],
        Resource: `arn:aws:secretsmanager:${region}:${accountId}:secret:NEW_RELIC_LICENSE_KEY*`,
      }
    ]
  },
  functions: {
    smsTrigger: {
      name: `mougi-sms-trigger-${stage}`,
      handler: 'handlers/trigger.handler',
      memorySize: 128,
      events: [
        {
          http: {
            method: 'post',
            path: '/send',
            // cors: true,
          }
        }
      ],
    }
  }
};

module.exports = serverlessConfiguration;
