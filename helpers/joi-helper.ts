import { AnySchema, ArraySchema, extend, ValidationOptions } from '@hapi/joi';
import { badData } from '@hapi/boom';
import { isEmpty } from 'lodash';

export const joiArrayConvertor = extend((joi: { array: () => ArraySchema }) => ({
  type: 'stringArray',
  base: joi.array(),
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  coerce: (value: string, helpers) => {
    if (isEmpty(value)) {
      return { value: [] };
    } else {
      return { value: (value.split ? value.split(',') : value) };
    }
  },
}));

export const validate = <T = any>(
  data: any, schema: AnySchema,
  options: ValidationOptions = {},
) => {
  const result = schema.validate(data, options);
  if (result.error) {
    throw badData(result.error.message);
  }
  if (result.errors) {
    throw badData(result.errors.message);
  }
  return result.value as T;
};
