/* eslint-disable @typescript-eslint/ban-types */
const getResponseHeaders = (isErr = false) => {
  const corsEnabled = (process.env.CORS_ENABLED === 'Yes');
  const headers = {
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    'Access-Control-Allow-Credentials': true
  };
  if (corsEnabled) {
    headers['Access-Control-Allow-Origin'] = process.env.CORS_ORIGIN;
  }
  if (isErr) {
    headers['Content-Type'] = 'application/json';
  }
  return headers;
};

// eslint-disable-next-line @typescript-eslint/ban-types
export const healthyResp = (body: object, headers = {}) => {
  const formattedHeaders = getResponseHeaders();
  Object.assign(formattedHeaders, headers);
  return {
    statusCode: 200,
    headers: formattedHeaders,
    body: JSON.stringify(body),
  };
};
