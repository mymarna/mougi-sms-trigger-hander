import 'source-map-support/register';

import { AccessHelper } from '@mougi/info';
import { APIGatewayEvent } from 'aws-lambda';
import AWS from 'aws-sdk';
import { healthyResp, validate } from '../helpers';
import Joi from '@hapi/joi';
import { handleError } from '../libraries/logging';

const sns = new AWS.SNS();

const sendBodySchema = Joi.object<ISendBody, ISendBody>().keys({
  phone: Joi.string().required(),
  message: Joi.string().required(),
  senderPhone: Joi.string().optional(),
});

interface ISendBody {
  phone: string;
  message: string;
  senderPhone?: string;
}

export const handler = async (event: APIGatewayEvent, context, callback) => {
  console.log('%j', event);
  context.callbackWaitsForEmptyEventLoop = false;

  try {
    const formattedBody = validate<ISendBody>(JSON.parse(event.body), sendBodySchema, { stripUnknown: true });

    AccessHelper.verify(event.headers['api-authorization']);
  
    const smsConfig: AWS.SNS.PublishInput = {
      Message: formattedBody.message,
      PhoneNumber: formattedBody.phone,
    };  
    if (formattedBody.senderPhone) {
      smsConfig.MessageAttributes = {
        'AWS.MM.SMS.OriginationNumber' : {
          DataType: 'String',
          StringValue: formattedBody.senderPhone,
        }
      };
    }
  
    const result = await sns.publish(smsConfig).promise();

    callback(null, healthyResp(result));
  } catch (err) {
    callback(null, handleError(err, event.requestContext.requestId));
  }
};
