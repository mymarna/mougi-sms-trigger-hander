import { isBoom } from '@hapi/boom';
import { stage } from '../config';

export const handleError = (err: any, traceId: string) => {
  console.error(err);
  if (isBoom(err)) {
    const errOutput = err.output;
    return {
      statusCode: errOutput.statusCode,
      body: JSON.stringify({
        code: errOutput.payload.error,
        message: errOutput.payload.message,
        extra: Object.assign({}, err.data, { traceId }),
      })
    };
  } else {
    let unexpectedErrorMsg = 'Internal server errors.';
    if (['tst', 'stg'].includes(stage)) {
      unexpectedErrorMsg = `error: ${err.message}, stack: ${err.stack}`;
    }
    return {
      statusCode: err.statusCode || 500,
      body: JSON.stringify({
        code: 'INTERNAL_ERROR',
        message: unexpectedErrorMsg,
        extra: { traceId },
      })
    };
  }
};
