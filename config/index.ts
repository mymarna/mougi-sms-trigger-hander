import { Environments } from '@mougi/info';
import * as dotenv from 'dotenv';
dotenv.config();

// only in prd env
export const stage = Environments.prd;
export const accountId = process.env.AWS_ACCOUNT_ID || '713583922704';
