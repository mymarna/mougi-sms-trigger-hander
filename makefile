test:
	env TS_NODE_PROJECT="tsconfig.json" mocha -r ts-node/register './{,!(node_modules)/**}/*.test.ts'

.PHONY: test build
